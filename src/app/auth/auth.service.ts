import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {HttpParams} from '@angular/common/http';
import {tokenNotExpired} from 'angular2-jwt';
import {Router} from '@angular/router';
import {NotificationsService} from 'angular2-notifications';
import {map} from 'rxjs/operators';


@Injectable()
export class AuthService {
    public token: string;

    constructor(private http: Http, private router: Router, private notificationService: NotificationsService) {
        // set token if saved in local storage
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(email: string, password: string): Observable<boolean> {
        const body = new HttpParams().set('email', email).set('password', password);
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post('http://localhost/to_do/public/api/auth/login', body.toString(), {headers})
            .pipe(
                map((response: Response) => {
                    // login successful if there's a jwt token in the response
                    const token = response.json() && response.json().data.token;
                    if (token) {
                        // set token property
                        this.token = token;


                        // store username and jwt token in local storage to keep user logged in between page refreshes
                        localStorage.setItem('token', token);
                        localStorage.setItem('username', email);

                        // return true to indicate successful login
                        return true;
                    } else {
                        // return false to indicate failed login
                        return false;
                    }
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    register(name: string, password: string, password_confirmation: string, email: string): Observable<boolean> {
        const body = new HttpParams()
            .set('name', name)
            .set('password', password)
            .set('password_confirmation', password_confirmation)
            .set('email', email);
        const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        return this.http.post('http://localhost/to_do/public/api/auth/register', body.toString(), {headers})
            .pipe(
                map((response: Response) => {
                    this.notificationService.success('success', response.json().message);
                    return true;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('token');
        localStorage.removeItem('username');

        this.router.navigate(['/']);
    }

    loggedIn() {
        return tokenNotExpired();
    }

}
