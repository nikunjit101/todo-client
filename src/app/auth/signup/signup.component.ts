import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthHttp} from 'angular2-jwt';
import {AuthService} from '../auth.service';
import {NgForm} from '@angular/forms';

declare var $: any;

@Component({
    selector: 'app-signup',
    moduleId: module.id,
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

    loading = false;
    error = '';
    thing: string;

    constructor(private router: Router, private authService: AuthService, public authHttp: AuthHttp) {
    }

    ngOnInit() {
        this.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);
    }

    checkFullPageBackgroundImage() {
        const $page = $('.full-page');
        const image_src = $page.data('image');

        if (image_src !== undefined) {
            const image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };

    register(form: NgForm) {
        this.loading = true;
        this.authService.register(form.value.name, form.value.password, form.value.password_confirmation, form.value.email)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['auth/signin']);
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            });
    }

}
