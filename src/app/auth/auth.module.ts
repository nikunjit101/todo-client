import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
// import { MdIconModule, MdCardModule, MdInputModule, MdCheckboxModule, MdButtonModule } from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthRoutes} from './auth.routing';

import {SigninComponent} from './signin/signin.component';
import {Http, RequestOptions} from '@angular/http';
import {AuthConfig, AuthHttp} from 'angular2-jwt';
import { SignupComponent } from './signup/signup.component';

// import { FlexLayoutModule } from '@angular/flex-layout';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
    return new AuthHttp(new AuthConfig({
        tokenName: 'token',
        tokenGetter: (() => localStorage.getItem('token')),
        globalHeaders: [{'Content-Type': 'application/x-www-form-urlencoded'}, {'Content-Type': 'application/json'}],
    }), http, options);
}


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AuthRoutes),
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        SigninComponent,
        SignupComponent
    ],
    providers: []
})

export class AuthModule {
}
