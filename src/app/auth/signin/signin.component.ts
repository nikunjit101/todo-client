import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AuthService} from '../auth.service';
import {NgForm} from '@angular/forms';
import {AuthHttp} from 'angular2-jwt';

declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

    loading = false;
    error = '';
    thing: string;

    constructor(private router: Router, private authService: AuthService, public authHttp: AuthHttp) {
    }

    ngOnInit() {
        this.checkFullPageBackgroundImage();

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);


        // reset login status
        this.authService.logout();
    }

    checkFullPageBackgroundImage() {
        const $page = $('.full-page');
        const image_src = $page.data('image');

        if (image_src !== undefined) {
            const image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    };


    login(form: NgForm) {
        this.loading = true;
        this.authService.login(form.value.email, form.value.password)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['dashboard']);

                    // this.authHttp.get('http://localhost:8000/secret')
                    //     .subscribe(
                    //         data => console.log(data),
                    //         err => console.log(err),
                    //         () => console.log('Request Complete')
                    //     );
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            });
    }
}
