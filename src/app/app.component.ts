import {Component, ElementRef, OnInit} from '@angular/core';

declare var $: any;

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {
    notificationObj: Object = {
        position: ['bottom', 'right'],
        animate: 'scale',
        timeOut: 3000,
        showProgressBar: false,
        pauseOnHover: true,
        preventDuplicates: false,
        clickToClose: true,
        clickIconToClose: true
    };

    constructor(private elRef: ElementRef) {
    }

    ngOnInit() {
        let body = document.getElementsByTagName('body')[0];
        var isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
        if (isWindows) {
            // if we are on windows OS we activate the perfectScrollbar function
            body.classList.add('perfect-scrollbar-on');
        } else {
            body.classList.add('perfect-scrollbar-off');
        }
        $.material.init();
    }
}
