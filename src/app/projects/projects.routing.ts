import {Routes} from '@angular/router';
import {ProjectListComponent} from './project-list/project-list.component';
import {ProjectAddComponent} from './project-add/project-add.component';
import {ProjectEditComponent} from './project-edit/project-edit.component';

export const ProjectsRoutes: Routes = [

    {path: '', component: ProjectListComponent},
    {path: 'create', component: ProjectAddComponent},
    {path: ':id/edit', component: ProjectEditComponent},
];
