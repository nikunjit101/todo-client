import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params} from '@angular/router';
import {Project} from '../project.model';
import {ProjectService} from '../project.service';

@Component({
    selector: 'app-project-edit',
    templateUrl: './project-edit.component.html',
    styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

    projectEditForm: FormGroup;
    selectedProject: Project;
    forbiddenProjects = [];

    constructor(private projectService: ProjectService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params
            .subscribe(
                (params: Params) => {
                    this.selectedProject = this.projectService.getProjectById(params['id']);
                }
            );

        this.projectEditForm = new FormGroup({
            'name': new FormControl(this.selectedProject.name, [Validators.required, this.forbiddenNames.bind(this)]),
            'color': new FormControl(this.selectedProject.color, Validators.required),
        });

        this.forbiddenProjects = this.projectService.getProjects();
    }

    onSubmit(): void {
        if (this.projectEditForm.valid) {
            const projectModel: Project = this.projectEditForm.value;
            projectModel.id = this.selectedProject.id;
            this.projectService.updateProject(projectModel);
        }
    }

    forbiddenNames(control: FormControl): { [s: string]: boolean } {
        if (this.forbiddenProjects.findIndex((project: Project) => project.name === control.value
                && project.id !== this.selectedProject.id) !== -1) {
            return {'nameIsForbidden': true};
        }
        return null;
    }

}
