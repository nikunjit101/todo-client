import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProjectAddComponent} from './project-add/project-add.component';
import {ProjectEditComponent} from './project-edit/project-edit.component';
import {ProjectListComponent} from './project-list/project-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import {DataTablesModule} from 'angular-datatables';
import {RouterModule} from '@angular/router';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {ProjectService} from './project.service';
import {ProjectsRoutes} from './projects.routing';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(ProjectsRoutes),
        DataTablesModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        }),
    ],
    declarations: [
        ProjectAddComponent,
        ProjectEditComponent,
        ProjectListComponent
    ],
    providers: [
        ProjectService
    ]
})
export class ProjectsModule {
}
