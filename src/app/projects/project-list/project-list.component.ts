import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';
import {DataTableDirective} from 'angular-datatables';
import {Project} from '../project.model';
import {ProjectService} from '../project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy, AfterViewInit {

    private projectsChangeSub: Subscription;

    private swalObj: object = {
        title: 'Are you sure?',
        text: 'You will not be able to recover this record!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
    };

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    // We use this trigger because fetching the list of persons can be quite long,
    // thus we ensure the data is fetched before rendering
    dtTrigger: Subject<any> = new Subject();

    projects: Project[] = [];

    constructor(private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.projectService.retrieveProjects();
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10
        };
        this.projectsChangeSub = this.projectService.projectsArrChanged
            .subscribe((projects: Project[]) => {
                    this.projects = projects;
                    this.renderDataTable();
                }
            );
    }

    deleteProject(id: number): void {
        this.projectService.deleteProject(id);
    }

    renderDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to re-render again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        this.projectsChangeSub.unsubscribe();
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

}
