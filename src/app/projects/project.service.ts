import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Router} from '@angular/router';
import {NotificationsService} from 'angular2-notifications/dist';
import {map} from 'rxjs/operators';
import {Response} from '@angular/http';
import {Subject} from 'rxjs/Subject';
import {AuthHttp} from 'angular2-jwt';
import {Project} from './project.model';

@Injectable()
export class ProjectService {

    projectsArrChanged = new Subject<Project[]>();

    private projects: Project[] = [];

    constructor(public authHttp: AuthHttp, public notificationService: NotificationsService, public router: Router) {
    }

    retrieveProjectsObservable(): Observable<Project[]> {
        return this.authHttp.get('http://localhost/to_do/public/api/projects')
            .pipe(
                map((response: Response) => {
                    // this.projects = response.json().data;
                    // return this.projects.slice() || {};
                    return response.json().data;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    deleteProjectObservable(id: number): Observable<number> {
        return this.authHttp.delete('http://localhost/to_do/public/api/projects/' + id)
            .pipe(
                map((response: Response) => {

                    this.notificationService.success('success', response.json().message);
                    return id;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    createProjectObservable(project: Project): Observable<Project> {
        return this.authHttp.post('http://localhost/to_do/public/api/projects', project)
            .pipe(
                map((response: Response) => {

                    this.notificationService.success('success', response.json().message);
                    return response.json().data;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    updateProjectObservable(project: Project): Observable<Project> {
        return this.authHttp.put('http://localhost/to_do/public/api/projects/' + project.id, project)
            .pipe(
                map((response: Response) => {

                    this.notificationService.success('success', response.json().message);
                    return response.json().data;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    retrieveProjects(): void {
        this.retrieveProjectsObservable().subscribe(projects => {
            this.projects = projects;
            this.projectsArrChanged.next(this.projects.slice());
        });
    }

    deleteProject(id: number): void {
        this.deleteProjectObservable(id).subscribe(projectId => {
            const index = this.projects.findIndex((project: Project) => project.id === projectId);
            if (index !== -1) {
                this.projects.splice(index, 1);
                this.projectsArrChanged.next(this.projects.slice());
            }
        });
    }

    addProject(projectData: Project): void {
        this.createProjectObservable(projectData).subscribe(project => {
            this.projects.push(project);
            this.projectsArrChanged.next(this.projects.slice());
            this.router.navigate(['projects']);
        });
    }

    updateProject(projectData: Project): void {
        this.updateProjectObservable(projectData).subscribe(updatedProject => {
            const index = this.projects.findIndex((project: Project) => project.id === updatedProject.id);
            if (index !== -1) {
                this.projects[index] = updatedProject;
                this.projectsArrChanged.next(this.projects.slice());
                this.router.navigate(['projects']);
            }
        });
    }

    getProjectById(id: number): Project {
        const index = this.projects.findIndex((project: Project) => project.id == id);
        if (index !== -1) {
            return this.projects[index];
        }
        return null;
    }

    getProjects(): Project[] {
        return this.projects.slice();
    }

}
