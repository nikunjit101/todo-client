import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProjectService} from '../project.service';
import {Project} from '../project.model';

@Component({
  selector: 'app-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.css']
})
export class ProjectAddComponent implements OnInit {

    projectAddForm: FormGroup;
    forbiddenProjects = [];

    constructor(private projectService: ProjectService) {
    }

    ngOnInit() {
        this.projectAddForm = new FormGroup({
            'name': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
            'color': new FormControl(null, Validators.required),
        });

        this.forbiddenProjects = this.projectService.getProjects();
    }

    onSubmit(): void {
        if (this.projectAddForm.valid) {
            this.projectService.addProject(this.projectAddForm.value as Project);
        }
    }

    forbiddenNames(control: FormControl): { [s: string]: boolean } {
        if (this.forbiddenProjects.findIndex((project: Project) => project.name === control.value) !== -1) {
            return {'nameIsForbidden': true};
        }
        return null;
    }
}
