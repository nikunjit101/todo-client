import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs/Subject';

import 'rxjs/add/operator/map';
import {Subscription} from 'rxjs/Subscription';
import {DataTableDirective} from 'angular-datatables';
import {User} from '../user.model';
import {UserService} from '../user.service';

@Component({
    selector: 'app-user-list',
    moduleId: module.id,
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy, AfterViewInit {
    private usersChangeSub: Subscription;

    private swalObj: object = {
        title: 'Are you sure?',
        text: 'You will not be able to recover this record!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
    };

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    // We use this trigger because fetching the list of persons can be quite long,
    // thus we ensure the data is fetched before rendering
    dtTrigger: Subject<any> = new Subject();

    users: User[] = [];

    constructor(private userService: UserService) {
    }

    ngOnInit(): void {
        this.userService.retrieveUsers();
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10
        };
        this.usersChangeSub = this.userService.usersArrChanged
            .subscribe((users: User[]) => {
                    this.users = users;
                    this.renderDataTable();
                }
            );
    }

    deleteUser(id: number): void {
        this.userService.deleteUser(id);
    }

    renderDataTable(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to re-render again
            this.dtTrigger.next();
        });
    }

    ngOnDestroy(): void {
        this.usersChangeSub.unsubscribe();
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

}
