import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {User} from '../user.model';

@Component({
    selector: 'app-user-add',
    templateUrl: './user-add.component.html',
    styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
    userAddForm: FormGroup;
    forbiddenUsers = [];

    constructor(private userService: UserService) {
    }

    ngOnInit() {
        this.userAddForm = new FormGroup({
            // 'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
            'name': new FormControl(null, Validators.required),
            'email': new FormControl(null, [Validators.required, Validators.email, this.forbiddenNames.bind(this)]),
            'is_admin': new FormControl(false),
            'password': new FormControl(null, Validators.required)
        });

        this.forbiddenUsers = this.userService.getUsers();
    }

    onSubmit(): void {
        if (this.userAddForm.valid) {
            this.userService.addUser(this.userAddForm.value as User);
        }
    }

    forbiddenNames(control: FormControl): { [s: string]: boolean } {
        if (this.forbiddenUsers.findIndex((user: User) => user.email === control.value) !== -1) {
            return {'nameIsForbidden': true};
        }
        return null;
    }


}
