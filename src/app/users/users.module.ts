import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {UsersRoutes} from './users.routing';
import {DataTablesModule} from 'angular-datatables';
import {UserService} from './user.service';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {UserAddComponent} from './user-add/user-add.component';
import {UserEditComponent} from './user-edit/user-edit.component';
import {UserListComponent} from './user-list/user-list.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(UsersRoutes),
        DataTablesModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        }),
    ],
    declarations: [
        UserAddComponent,
        UserEditComponent,
        UserListComponent
    ],
    providers: [
        UserService
    ]
})
export class UsersModule {
}
