import {Routes} from '@angular/router';
import {UserAddComponent} from './user-add/user-add.component';
import {UserListComponent} from './user-list/user-list.component';
import {UserEditComponent} from './user-edit/user-edit.component';

export const UsersRoutes: Routes = [

    // {
    //     path: '',
    //     children: [
    //         {path: '', component: UserListComponent},
    //         {path: 'create', component: UserAddComponent},
    //         {path: ':id/edit', component: UserEditComponent},
    //     ]
    // }

    {path: '', component: UserListComponent},
    {path: 'create', component: UserAddComponent},
    {path: ':id/edit', component: UserEditComponent},
];
