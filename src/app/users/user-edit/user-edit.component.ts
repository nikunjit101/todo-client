import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../user.service';
import {User} from '../user.model';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

    userEditForm: FormGroup;
    selectedUser: User;
    forbiddenUsers = [];

    constructor(private userService: UserService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params
            .subscribe(
                (params: Params) => {
                    this.selectedUser = this.userService.getUserById(params['id']);
                }
            );

        this.userEditForm = new FormGroup({
            // 'username': new FormControl(this.selectedUser.username, [Validators.required, this.forbiddenNames.bind(this)]),
            'name': new FormControl(this.selectedUser.name, Validators.required),
            'email': new FormControl(this.selectedUser.email, [Validators.required, Validators.email, this.forbiddenNames.bind(this)]),
            // 'mobile': new FormControl(this.selectedUser.mobile, Validators.required),
            // 'gender': new FormControl(this.selectedUser.gender),
            'is_admin': new FormControl(this.selectedUser.is_admin),
            // 'password': new FormControl(null, Validators.required)
        });

        this.forbiddenUsers = this.userService.getUsers();
    }

    onSubmit(): void {
        if (this.userEditForm.valid) {
            const userModel: User = this.userEditForm.value;
            userModel.id = this.selectedUser.id;
            this.userService.updateUser(userModel);
        }
    }

    forbiddenNames(control: FormControl): { [s: string]: boolean } {
        if (this.forbiddenUsers.findIndex((user: User) => user.email === control.value && user.id !== this.selectedUser.id) !== -1) {
            return {'nameIsForbidden': true};
        }
        return null;
    }

}
