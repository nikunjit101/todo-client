export class User {
    public id: number;
    public name: string;
    public email: string;
    public image: string;
    public is_admin: boolean;
    public is_active: boolean;

    constructor(id: number, name: string, email: string, image: string, is_admin: boolean, is_active: boolean) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.image = image;
        this.is_admin = is_admin;
        this.is_active = is_active;
    }
}
