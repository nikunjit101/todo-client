import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Response} from '@angular/http';
import {AuthHttp} from 'angular2-jwt';
import {NotificationsService} from 'angular2-notifications/dist';
import {User} from './user.model';
import {map} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import {Router} from '@angular/router';

@Injectable()
export class UserService {

    usersArrChanged = new Subject<User[]>();

    private users: User[] = [];

    constructor(public authHttp: AuthHttp, public notificationService: NotificationsService, public router: Router) {
    }

    retrieveUsersObservable(): Observable<User[]> {
        return this.authHttp.get('http://localhost/to_do/public/api/users')
            .pipe(
                map((response: Response) => {
                    // this.users = response.json().data;
                    // return this.users.slice() || {};
                    return response.json().data;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    deleteUserObservable(id: number): Observable<number> {
        return this.authHttp.delete('http://localhost/to_do/public/api/users/' + id)
            .pipe(
                map((response: Response) => {

                    this.notificationService.success('success', response.json().message);
                    return id;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    createUserObservable(user: User): Observable<User> {
        return this.authHttp.post('http://localhost/to_do/public/api/users', user)
            .pipe(
                map((response: Response) => {

                    this.notificationService.success('success', response.json().message);
                    return response.json().data;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    updateUserObservable(user: User): Observable<User> {
        return this.authHttp.put('http://localhost/to_do/public/api/users/' + user.id, user)
            .pipe(
                map((response: Response) => {

                    this.notificationService.success('success', response.json().message);
                    return response.json().data;
                })
            )
            .catch((err: Response) => {

                this.notificationService.error('Error', err.json().message);
                return Observable.throw(err);
            });
    }

    retrieveUsers(): void {
        this.retrieveUsersObservable().subscribe(users => {
            this.users = users;
            this.usersArrChanged.next(this.users.slice());
        });
    }

    deleteUser(id: number): void {
        this.deleteUserObservable(id).subscribe(userId => {
            const index = this.users.findIndex((user: User) => user.id === userId);
            if (index !== -1) {
                this.users.splice(index, 1);
                this.usersArrChanged.next(this.users.slice());
            }
        });
    }

    addUser(userData: User): void {
        this.createUserObservable(userData).subscribe(user => {
            this.users.push(user);
            this.usersArrChanged.next(this.users.slice());
            this.router.navigate(['users']);
        });
    }

    updateUser(userData: User): void {
        this.updateUserObservable(userData).subscribe(updatedUser => {
            const index = this.users.findIndex((user: User) => user.id === updatedUser.id);
            if (index !== -1) {
                this.users[index] = updatedUser;
                this.usersArrChanged.next(this.users.slice());
                this.router.navigate(['users']);
            }
        });
    }

    getUserById(id: number): User {
        const index = this.users.findIndex((user: User) => user.id == id);
        if (index !== -1) {
            return this.users[index];
        }
        return null;
    }

    getUsers(): User[] {
        return this.users.slice();
    }
}
